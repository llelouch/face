# Install requirements and environments

'''
conda create -n yourenvname python=3.7.11 anaconda
source activate yourenvname
'''

# Django preperation

'''
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
'''