from django.conf.urls import url
from django.urls import include, path
from detect import views

urlpatterns = [
	url('accounts/', include("django.contrib.auth.urls")),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('dashboard/#', views.helper, name="help"),
    # path('dashboard/cam', views.camera, name="cam"),
    path('dashboard/cam', views.livefe, name='cam'),
    path('dashboard/ca', views.ca, name="ca"),
    path('mylist/list', views.list, name="list"),
    path('mylist/#', views.lists, name="lists"),
    path('mylist', views.mylist, name="mylist"),
    path('mylist/<int:list_id>', views.del_list, name="del_list"),
    path('chart/', views.chart, name="chart"),
    path('chart/#', views.chartData, name="chartData"),
]