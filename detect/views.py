from django.shortcuts import render

# Create your views here.

import datetime
import json
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views import generic
from django.views.decorators import gzip
from django.http import StreamingHttpResponse
from .models import Genre, VideoCamera, VideoCamera1, PlayList, ListSongs, emotion

from django.shortcuts import redirect, render
from django.urls import reverse
from django.shortcuts import render
from detect.forms import UserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .spotify import get_Music
from django.db import connection
from datetime import datetime



def dashboard(request):
    crap = ['shjdbcjsfd', 'dfjkvfb', 'dfkjfvb']
    return render(request, "detect/dashboard.html")


def mylist(request):
    crap = ['shjdbcjsfd', 'dfjkvfb', 'dfkjfvb']
    return render(request, "detect/playlist.html")


def chart(request):
    return render(request, "detect/chart.html")


def chartData(request):
    user_id = request.user.id
    date = datetime.now().date()
    year = datetime.now().year
    month = datetime.now().month
    y = str(year)
    ym = y + "-0" + str(month)
    ymm = str(month)
    ymd = str(date)
    cursor = connection.cursor()
    cursor.execute("SELECT emotion FROM detect_emotion as l WHERE user_id = %s and YEAR(date) = %s", (user_id, y))
    a = cursor.fetchall()
    yearData = dataCol(a, y, user_id)
    cursor.execute("SELECT emotion FROM detect_emotion as l WHERE user_id = %s and MONTH(date) = %s", (user_id, ymm))
    a = cursor.fetchall()
    monthData = dataCol(a, ym, user_id)
    cursor.execute("SELECT emotion FROM detect_emotion as l WHERE user_id = %s and date = %s", (user_id, ymd))
    a = cursor.fetchall()
    dayData = dataCol(a, ymd, user_id)
    data = {
        "y": yearData,
        "m": monthData,
        "d": dayData,
    }
    return HttpResponse(
        json.dumps(data),
        content_type="application/json"
    )


def dataCol(a, label, user_id):
    labels = [
        'Neutral',
        'Happy',
        'Sad',
        'Angry',
        'Fear',
        'Surprise',
        'Disgust'
    ]
    chartLabel = label
    chartdata = []
    n = 0
    h = 0
    sa = 0
    an = 0
    f = 0
    su = 0
    d = 0
    for x in a:
        if x == ('Neutral',):
            n += 1
        elif x == ('Sad',):
            sa += 1
        elif x == ('Happy',):
            h += 1
        elif x == ('Angry',):
            an += 1
        elif x == ('Fear',):
            f += 1
        elif x == ('Surprise',):
            su += 1
        elif x == ('Disgust',):
            d += 1
    chartdata.append(n)
    chartdata.append(h)
    chartdata.append(sa)
    chartdata.append(an)
    chartdata.append(f)
    chartdata.append(su)
    chartdata.append(d)
    data = {
        "labels": labels,
        "chartLabel": chartLabel,
        "chartdata": chartdata,
    }
    return data


def list(request):
    if request.method == 'POST':
        mlist = json.loads(request.POST.get('data'))
        user_id = request.user.id
        play_list = PlayList.objects.create(user_id=user_id, name=mlist['name'])
        play_list.save()
        emotion_type = emotion.objects.create(emotion=mlist['emotion'], user_id=user_id, date=datetime.today())
        emotion_type.save()
        new_list = mlist['list']
        for x, item in enumerate(new_list['artist']):
            print(new_list['artist'][x])
            list_songs = ListSongs.objects.create(list_id=play_list.id, song_name=new_list['name'][x],
                                                  artist_name=new_list['artist'][x], genre=new_list['genre'][x],
                                                  link=new_list['album'][x])
            list_songs.save()
            pass
        return HttpResponse("saved!")

    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )


def lists(request):
    if request.method == 'GET':
        data = {}
        lists = []
        list1 = []
        list2 = []
        user_id = request.user.id
        cursor = connection.cursor()
        cursor.execute(
            "SELECT p.name, song_name, artist_name, genre, link, p.id FROM detect_listsongs as l LEFT JOIN detect_playlist as p ON l.list_id = p.id WHERE user_id = %s",
            (user_id))
        a = cursor.fetchall()
        b = a[0][0]
        c = len(a)
        for i, l in enumerate(a):
            for j, m in enumerate(l):
                list2.append(m)
            list1.append(list2)
            list2 = []
            if i + 1 < c:
                if b != a[i + 1][0]:
                    b = a[i + 1][0]
                    lists.append(list1)
                    list1 = []
            else:
                lists.append(list1)
                list1 = []
                break
        for i in lists:
            name = i[0][0]
            body = i
            d = {name: body}
            data.update(d)
    return HttpResponse(
        json.dumps(data),
        content_type="application/json"
    )


def del_list(request, list_id):
    print(list_id)
    item = PlayList.objects.get(pk=list_id)
    item.delete()
    return redirect('mylist')


def helper(request):
    if request.method == 'GET':
        param1 = request.GET.get('param_first')
        response_data = 'successful!'
        a = get_Music(param1)
        artist = a.artist_name
        name = a.track_name
        album = a.album
        genre = a.genre
        json_data = {
            "artist": artist,
            "name": name,
            "album": album,
            "genre": genre,
        }
        print(json_data)
        return HttpResponse(
            json.dumps(json_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )


def fuck(request):
    return render(request, "Detect/fuck.html")


@login_required
def special(request):
    return HttpResponse("You are logged in !")


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('Detect/dashboard.html'))


def register(request):
    registered = False
    user_form = UserForm(data=request.POST)
    if request.method == "POST":
        if request.POST.get('submit') == 'Log in':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('dashboard'))
                else:
                    return HttpResponse("Your account was inactive.")
            else:
                print("Someone tried to login and failed.")
                print("They used username: {} and password: {}".format(username, password))
                return HttpResponse("Invalid login details given")
        elif request.POST.get('submit') == 'Sign up':
            # form = CustomUserCreationForm(request.POST)
            if user_form.is_valid():
                user = user_form.save()
                user.set_password(user.password)
                user.save()
                registered = True
                return HttpResponseRedirect(reverse('dashboard'))
            else:
                print(user_form.errors)
    elif request.method == 'GET':
        if request.GET.get('submit') == 'Log in':
            return render(request, 'Detect/register.html', {})
        elif request.GET.get('submit') == 'Sign up':
            user_form = UserForm()
    return render(request, 'Detect/register.html',
                  {'user_form': user_form,
                   'registered': registered})


def gen(camera):
    while True:
        frame = camera.get_frame(True)
        emo = camera.p
        # print(emo)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


# @gzip.gzip_page
def livefe(request):
    print(request.method)
    emo = ""
    if request.method == 'GET':
        try:
            cam = VideoCamera()
            resp = StreamingHttpResponse(gen(cam), content_type="multipart/x-mixed-replace;boundary=frame")
            return resp
        except:  # This is bad! replace it with proper handling
            return emo
            pass
    elif request.method == 'HEAD':
        print("success")
        a = True
        camera = VideoCamera1()
        while a == True:
            emo = camera.get_frame(True)
            a = False
            print(emo)
            json_data = {
                'emo': emo,
            }
            return HttpResponse(emo)

    else:
        print("fuck")
        return HttpResponse(
            json.dumps({"emo": emo}),
            content_type="application/json"
        )


def ca(request):
    if request.method == 'GET':
        print("success")
        a = True
        camera = VideoCamera1()
        while a == True:
            emo = camera.get_frame(True)
            a = False
            print(emo)
            json_data = {
                'emo': emo,
            }
            return HttpResponse(emo)
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )